context("Applay", () => {
  beforeEach(() => {
    cy.createNewInstance();
  });

  it("should apply for job offert", () => {
    cy.loginAsEmpolyee();

    cy.get("app-search").within(() => {
      cy.get("#searcher").type(">0PLN{enter}");
      cy.get(".search-icon-container > .svg-inline--fa").click();
    });

    cy.get("app-search-results app-employee-job-offer-tab")
      .eq(5)
      .within(() => cy.get(".btn.btn-light").click());

    cy.get(".h3").as("title");

    cy.get("form").within(() => {
      cy.fixture("cv.pdf").then((content) => {
        cy.get(".form-control-file").attachFile({
          fileContent: content.toString(),
          fileName: "cy.pdf",
          mimeType: "application/pdf",
        });
      });
      cy.get(".btn").click();
    });

    cy.get("@title").then((title) => {
      cy.get("app-employee-job-offer-tab .job-name-title")
        .contains(title.text().trim())
        .should("have.length", 1);
    });
  });
});
