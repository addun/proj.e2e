import faker from "faker";

context("user", () => {
  beforeEach(() => {
    cy.createNewInstance();
    cy.visit("/");
    cy.loginAsEmpolyee();

    cy.get("#employeeNavbarDropdown").click();
    cy.get('[routerlink="employee"]').click();
  });

  it("should add new language", () => {
    cy.get("app-languages").within((container) => {
      const before = container.find(".languages-container").children().length;

      cy.get(".edit-btn").click();
      cy.fixture("languages").then((languages) => {
        cy.getFormControl("languageName").type(
          faker.random.arrayElement(languages)
        );
      });
      cy.getFormControl("level").select("3");
      cy.get(".btn-primary").click().wait(500);

      cy.get(".languages-container")
        .find(".row")
        .should("have.length", before + 1);
    });
  });

  it("should remove language", () => {
    cy.get("app-languages .languages-container")
      .find(".row")
      .its("length")
      .as("before");

    cy.get("app-languages .icon-danger > .svg-inline--fa > path")
      .first()
      .click();
    cy.get("app-employee-modal .btn-danger").click();

    cy.wait(500);

    cy.get("app-languages").within((container) => {
      const after = container.find(".languages-container").children().length;
      cy.get<number>("@before").should("eq", after + 1);
    });
  });

  it("should add job experience", () => {
    cy.get("app-professional-experience").within(() => {
      cy.root()
        .find(".job-container")
        .children()
        .its("length")
        .as("lengthBefore");

      cy.get(".profile-section-heading .edit-btn").click();

      cy.getFormControl("position").type("Junior developer");
      cy.getFormControl("country").type("Polska");
      cy.getFormControl("city").type("Rzeszow");
      cy.getFormControl("companyName").type("Comatch");
      cy.getFormControl("startMonth").type("10");
      cy.getFormControl("startYear").type("2015");
      cy.getFormControl("endMonth").type("10");
      cy.getFormControl("endYear").type("2017");
      cy.getFormControl("jobDescription").type("Lorem ipsum");

      cy.get(".edit-mode-btns-area .btn-primary").click().wait(500);

      cy.root()
        .find(".job-container")
        .children()
        .its("length")
        .then((after) => {
          cy.get<number>("@lengthBefore").should("equal", after - 1);
        });
    });
  });

  it("should add education", () => {
    cy.get("app-education").within(() => {
      cy.root()
        .find(".education-history-container")
        .children()
        .its("length")
        .as("lengthBefore");

      cy.get(".profile-section-heading > .edit-btn").click();
      cy.getFormControl("schoolName").type("Cracow");
      cy.getFormControl("branchOfStudy").type("UP");
      cy.getFormControl("speciality").type("IT");
      cy.getFormControl("degree").type("Hight");
      cy.getFormControl("startYear").type("2010");
      cy.getFormControl("endYear").type("2015");

      cy.get(".edit-mode-btns-area .btn-primary").click().wait(500);

      cy.root()
        .find(".education-history-container")
        .children()
        .its("length")
        .then((after) => {
          cy.get<number>("@lengthBefore").should("equal", after - 1);
        });
    });
  });

  it("should add certificate", () => {
    cy.get("app-certificates").within(() => {
      cy.root()
        .find(".certificates")
        .children()
        .its("length")
        .as("lengthBefore");

      cy.get(".profile-section-heading > .edit-btn").click();
      cy.getFormControl("certificateName").type("Cracow");
      cy.getFormControl("certificateOrganizerName").type("UP");
      cy.getFormControl("monthOfObtain").type("5");
      cy.getFormControl("yearOfObtain").type("2018");

      cy.get(".edit-mode-btns-area .btn-primary").click().wait(500);

      cy.root()
        .find(".certificates")
        .children()
        .its("length")
        .then((after) => {
          cy.get<number>("@lengthBefore").should("equal", after - 1);
        });
    });
  });
});
