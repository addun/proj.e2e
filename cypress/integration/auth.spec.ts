import faker from "faker";

context("Auth", () => {
  beforeEach(() => {
    cy.createNewInstance();
  });

  it("should create employee account", () => {
    const password = faker.internet.password(15) + "_aA1";

    const data = {
      email: faker.internet.email(),
      password,
      repeatPassword: password,
      name: "Name",
      surname: "surname",
      birthDate: "2021-02-17",
      sex: "man",
      phone: "666666666",
      country: "Polska",
      city: "Warszawa",
      websiteUrl: "www.brak.pl",
      about: "Jestem",
    };

    cy.get(".nav-item:nth-child(1) > .nav-link").click();
    cy.get(".col-6:nth-child(1) .btn").click();

    cy.get("app-register-signin-data").within(() => {
      cy.getFormControl("email").type(data.email);
      cy.getFormControl("password").type(data.password);
      cy.getFormControl("passwordConfirm").type(data.password);
      cy.get(".btn-success").click();
    });

    cy.get("app-register-personal-data").within(() => {
      cy.getFormControl("name").type(data.name);
      cy.getFormControl("surname").type(data.surname);
      cy.getFormControl("birthDate").type(data.birthDate);
      cy.getFormControl("sex").select(data.sex);
      cy.getFormControl("phone").type(data.phone);
      cy.getFormControl("country").type(data.country);
      cy.getFormControl("city").type(data.city);
      cy.getFormControl("websiteUrl").type(data.websiteUrl);
      cy.getFormControl("about").type(data.about);
      cy.get(".btn-success").click();
    });

    cy.get("app-registration-summary")
      .should("contain", data.name)
      .should("contain", data.surname)
      .should("contain", data.email)
      .should("contain", data.phone)
      .should("contain", data.websiteUrl)
      .should("contain", data.birthDate)
      .should("contain", data.sex === "man" ? "mężczyzna" : "kobieta")
      .find(".btn-success")
      .click();

    cy.get("app-operation-success").within(() => {
      cy.get(".btn-success").click();
    });

    cy.get("#employeeLoginTab").click();
    cy.get("#employeeLogin").within(() => {
      cy.getFormControl("email").type(data.email);
      cy.getFormControl("password").type(data.password);
      cy.get(".btn-success").click();
    });

    cy.get("app-edit-personal-data").within(() => {
      cy.get(".personal-data-row")
        .should("contain", data.name)
        .should("contain", data.surname)
        .should("contain", data.email)
        .should("contain", data.phone)
        .should("contain", data.websiteUrl)
        .should("contain", data.birthDate)
        .should("contain", data.sex === "man" ? "mężczyzna" : "kobieta");
    });
  });

  it("should create employer account", () => {
    const password = faker.internet.password(15) + "_aA1";

    const data = {
      email: faker.internet.email(),
      password,
      repeatPassword: password,
      companyName: faker.random.uuid(),
      street: faker.address.streetName(),
      companyDescription: faker.lorem.paragraph(),
      apartmentNumber: faker.random.number(150) + "",
      flatNumber: faker.random.number(10) + "",
      city: faker.address.city(),
      postalCode: faker.address.zipCode("##-###"),
      foundedYear: faker.random.number({ min: 1900, max: 2020 }) + "",
      companySize: faker.random.number({ min: 1, max: 2500 }) + "",
      nip: "7017776849",
      websiteUrl: faker.internet.url(),
      country: "Polska",
    };

    cy.get(".nav-item:nth-child(1) > .nav-link").click();
    cy.get(".col-6:nth-child(2) .btn").click();

    cy.get("app-register-signin-data").within(() => {
      cy.getFormControl("email").type(data.email);
      cy.getFormControl("password").type(data.password);
      cy.getFormControl("passwordConfirm").type(data.password);
      cy.get(".btn-success").click();
    });

    cy.get("app-register-company-data").within(() => {
      cy.getFormControl("companyName").type(data.companyName);
      cy.getFormControl("street").type(data.street);
      cy.getFormControl("companyDescription").type(data.companyDescription);
      cy.getFormControl("apartmentNumber").type(data.apartmentNumber);
      cy.getFormControl("flatNumber").type(data.flatNumber);
      cy.getFormControl("city").type(data.city);
      cy.getFormControl("postalCode").type(data.postalCode);
      cy.getFormControl("country").type(data.country);
      cy.getFormControl("foundedYear").type(data.foundedYear);
      cy.getFormControl("companySize").type(data.companySize);
      cy.getFormControl("nip").type(data.nip);
      cy.getFormControl("websiteUrl").type(data.websiteUrl);
      cy.get(".btn-success").click();
    });

    cy.get("app-registration-summary")
      .should("contain", data.companyName)
      .should("contain", data.street)
      .should("contain", data.companyDescription)
      .should("contain", data.apartmentNumber)
      .should("contain", data.flatNumber)
      .should("contain", data.city)
      .should("contain", data.postalCode)
      .should("contain", data.country)
      .should("contain", data.foundedYear)
      .should("contain", data.companySize)
      .should("contain", data.nip)
      .should("contain", data.websiteUrl)
      .find(".btn-success")
      .click();

    cy.get("app-operation-success a").click();
    cy.get("#employerLoginTab").click();

    cy.get("#employerLoginTab").click();
    cy.get("#employerLogin").within(() => {
      cy.getFormControl("email").type(data.email);
      cy.getFormControl("password").type(data.password);
      cy.get(".btn-success").click();
    });

    cy.get("app-company-data")
      .should("contain", data.companyName)
      .should("contain", data.email)
      .should("contain", data.foundedYear)
      .should("contain", data.companySize)
      .should("contain", data.nip)
      .should("contain", data.websiteUrl);

    cy.get("app-company-locations")
      .should("contain", data.country)
      .should("contain", data.city)
      .should("contain", data.postalCode)
      .should("contain", data.street)
      .should("contain", data.apartmentNumber)
      .should("contain", data.flatNumber);
  });
});
