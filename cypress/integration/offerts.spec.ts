import * as faker from "faker";

context("Offerts", () => {
  beforeEach(() => {
    cy.createNewInstance();
  });
  
  it("should add job offert", () => {
    cy.loginAsEmpolyer();

    cy.fixture<string[]>("technologies").then((technologies) => {
      const data = {
        title: faker.random.word(),
        country: faker.address.country(),
        city: faker.address.city(),
        salaryFrom: faker.random.number({ min: 1000, max: 2500 }),
        salaryTo: faker.random.number({ min: 5000, max: 8000 }),
        currency: "PLN",
        experienceLevel: faker.random.arrayElement(["JUNIOR"]),
        workTime: faker.random.words(3),
        invoiceType: faker.random.words(5),
        address: faker.random.words(3),
        remote: faker.random.words(2),
        desc: faker.random.words(25),
        technologies: faker.random.arrayElements(technologies, 5),
      };

      cy.get("#employerNavbarDropdown").click();
      cy.get('[routerlink="employer/job-offers"]').click();
      cy.get(".btn-success").click();

      console.log(data);

      cy.get("app-add-job-offer").within(() => {
        cy.getFormControl("title").type(data.title);
        cy.getFormControl("country").type(data.country);
        cy.getFormControl("city").type(data.city);
        cy.getFormControl("salaryFrom").type(data.salaryFrom + "");
        cy.getFormControl("salaryTo").type(data.salaryTo + "");
        cy.getFormControl("currency").select(data.currency);
        cy.getFormControl("experienceLevel").select(data.experienceLevel);
        cy.getFormControl("workTime").type(data.workTime);
        cy.getFormControl("invoiceType").type(data.invoiceType);
        cy.getFormControl("address").type(data.address);
        cy.getFormControl("remote").type(data.remote);
        cy.get("#editor").type(data.desc);
        cy.get(
          "form > div:nth-child(2) > div:nth-child(2) > div:nth-child(2)"
        ).within(() => {
          data.technologies.forEach((element) => {
            cy.get("input").type(element);
            cy.get("button").click();
          });
        });

        cy.wait(1000);

        cy.get(".edit-mode-btns-area .btn-primary").click();
      });

      cy.get("app-job-offer-tab", { timeout: 5000 });

      cy.get(".job-name-title")
        .contains(data.title)
        .closest("app-job-offer-tab")
        .find(".btn-light")
        .click();

      cy.get("app-job-applicants")
        .should("contain", data.city)
        .should("contain", data.address)
        .should("contain", data.remote)
        .should("contain", data.salaryFrom)
        .should("contain", data.salaryTo)
        .should("contain", data.desc)
        .should("contain", new Date().toISOString().slice(0, 10))
        .should("contain", data.invoiceType)
        .within(() => {
          data.technologies.forEach((t) => cy.root().should("contain", t));
        });
    });
  });

  it("should search job offert", () => {
    cy.get("app-search").within(() => {
      cy.get("#searcher").type("sql{enter}");
      cy.get(".search-icon-container > .svg-inline--fa").click();
    });

    cy.get("app-search-results").within(() => {
      cy.get("app-employee-job-offer-tab .skill-tags").should(
        "contain",
        "SQL",
        { matchCase: false }
      );
    });
  });
});
