/// <reference types="cypress" />

declare namespace Cypress {
  interface Chainable {
    getFormControl(value: string): Chainable<Element>;
    loginAsEmpolyer(email?: string, password?: string): Chainable<Element>;
    loginAsEmpolyee(email?: string, password?: string): Chainable<Element>;
    createNewInstance(): Chainable<Element>;
  }
}

Cypress.Commands.add("getFormControl", (value) => {
  cy.get(`[formControlName="${value}"]`);
});

Cypress.Commands.add("createNewInstance", () => {
  cy.clearCookies();
  cy.clearLocalStorage();
  cy.window().then((win) => win.sessionStorage.clear());
  cy.visit("/");
});

Cypress.Commands.add("loginAsEmpolyer", (email?: string, password?: string) => {
  cy.get(".nav-item:nth-child(2) > .nav-link").click();

  cy.get("#employerLoginTab").click();
  cy.get("#employerLogin").within(() => {
    cy.getFormControl("email").type(email || "Lurline_Willms@yahoo.com");
    cy.getFormControl("password").type(password || "koEkuaYg2_SZn2h_aA1");
    cy.get(".btn-success").click();
  });

  cy.visit("/");
});

Cypress.Commands.add("loginAsEmpolyee", (email?: string, password?: string) => {
  cy.get(".nav-item:nth-child(2) > .nav-link").click();

  cy.get("#employeeLoginTab").click();
  cy.get("#employeeLogin").within(() => {
    cy.getFormControl("email").type(email || "Olin_Connelly39@hotmail.com");
    cy.getFormControl("password").type(password || "q0aiTqb4DU80u_x_aA1");
    cy.get(".btn-success").click();
  });

  cy.visit("/");
});
